---
layout: default
title:  Retrospective
date:   2017-03-16 18:39:41 +0000
---

## What Went Well:
* We were all able to meet together and get work done.
* All of our work is done on time.
* We completely understand our project.
* Our project is on track.


## What Didn't Work:
* Communication is common or timely.
* We are lacking in the skill level required for the project.
* What we were required to do for the project was not well explained to all memebers of the team.

## Questions:
* How are we going to be mangaging our workload?
* How will we find time to work on the project?