---
layout: default
title:  Project Charter
date:   2017-02-09 18:39:41 +0000
---

# Team Charter

## Who are we?  
 Samantha Williams- Student
 
 Austin McGee- Student
 
 Blake Smith- Student
 
 Gerald Gannod- Instructor
 
 Cookeville Putnam County Animal Shelter- Customer
 
## How will we communicate?
 We will use Slack and Trello as a way to notify each others about concerns or updates.
 
## How will we make decisions?
 We will make decisions as a team with customer in mind. We will take into account the information we currently have from the customer and make a rational group decision.

## How will we handle conflict?
 We will evaluate each persons problem equally and then decide on a solution.
 
## How will we run our meetings?
 We are able to meet on Mondays, before and after class, and on Thursday. If the need arises, we can always get together and decide on a time and place during the week other than just Monday and Thursday. We plan to just get together and immediatly get work done and helping each other if there is a problem.

## How will we manage our work?
 We will use our resources such as slack and trello to make sure that we communicate on progress, potential problems, and meeting times that we work together on the project. Also, (Something we are already doing) we will send a brief message about our progresses we have made when we sign-off and stop working on our current project section. i.e. send a slack message saying what you have left on that section of the project.

## How will we have fun?
 We feel like if we make sure to communicate to one another and make our meetings interesting such as, finding interesting places to meet, or visiting the shelter, or just making sure that it isnt always 100% serious work. As long as we stay on top of our work and don't get behind, we can incorporate fun into our project!
 
## How will we improve our processes? 
 We will look at the quality of the work we create and decide if the quality is okay. If it is not, then the process we used to create that work will need to be changed so that the quality of that work is increased.
 
## What is our working agreement?
 We will divide up the work according to each person's skills and experience. If there is any problem in getting things done, we will let each person know about the problem and all work together to solve it.



# Project Charter
 
## Elevator Statement

**For:** Those who are looking to adopt, have found/lost an animal, and volunteers.

**Who:** Need to find out more information about the shelter, lost/found animals, shelter "Inventory", and "Happy Tales"

**The:** CPCAS Website Is A: Tool for organization and information

**That:** Allows for daily management of animals, volunteers, and donations,

**Unlike:** Volgistics, Facebook, or Petfinder

**Our Solution:** Is to bring all these focus together in a user friendly platform.

## Sliders

1-4

1 being least importance and 4 being most improtance

**Time:** 4

**Quality:** 3

**Scope:** 2

**Cost:** 1


## Challenges / Roadblocks / Risks 
* Lack of experience
* Other classes we are taking
* Customer may not attending each meetings

## Definition of Done
 Done would be when something has been signed off by the customer and approved for launch. 
 
## Milestones
* Mock-up created
* Basic website with contact information up and running
* Added ability to submit forms
* Login system up and running
* Added ability for users to schedule 
