---
layout: default
title:  Release Plan
date:   2017-03-08 18:39:41 +0000
---

# Iteration 2

## Accessibility

### Location (4)

* Display the location of CPCAS on the front page of the website.


### Hours (2)

* Display the overall times for when CPCAS is open.


### Contact Info (2)

* Display the contact info of CPCAS on the website.


## Resources 

### Local Vets (4)

* Have a page that displays the available local vets in Cookeville.

### Local Hospitals (4)

* Have a page that displays the available local animal hospitals in Cookeville.

### Rescues (4)

* Have a page that displays a list of animal resuces in Cookeville.

### Animal Control (4)

* Have a page that displays a list of the animal control officers in Cookeville.


## Donation

### Online Donation: PayPal (4)

* Have a service that allows users to donate online through CPCAS's Paypal account.

### Sponsorship Opportunities (2)

* Have a page that displays sponsorship opportunities such as adoption fees, spayed/neuter, etc.

### How to Volunteer: "Donate your time!" (8)

* Have a page that decribes how to volunteer at CPCAS.

### List of Consumables Needed (2)

* Have a page that displays a list of consumable needs, such as pet food, cleaning products, etc.


<br /><br /><br />
# Iteration 3

## Lost and Found: Public

### Create Account (16)

* Create a service that allows staff members at CPCAS to create accounts for the website.

### Post Lost Animals (8)

* Allow users to fill out a form online where the user list information about a lost animal, and a staff member will choose to post it the website.

### Post Found Animals (8)

* Allow users to fill out a form online where the user list information about a found animal, and a staff member will choose to post it the website.

### View Current Lost Animals (8)

* Have a page that displays all current information for lost animals.

### View Current Found Animals (8)

* Have a page that displays all current information for animals turned in to CPCAS.

