---
layout: default
title:  User Stories
date:   2017-02-16 18:39:41 +0000
---

# Must Have

## Accessibility

### Location (4)

* As a Member of the Community I would like to have the location of CPCAS on the Website as well as directions so that I won't be as confused as to where it is located.


### Hours (2)

* As a Member of the Community I would like to see the hours that CPCAS is open on the website so that I won't be as confused as to when it is open.


### Contact Info (2)

* As a Member of the Community I would like to have the contact information on the Website so that I will be able to easily contact CPCAS via phone, email, etc.


## Resources

### Local Vets (4)

* As a Member of the Community I would like to be able to find a list of local vets in the Community on CPCAS website so that I won't have to call and ask CPCAS to do my vaccinations.

### Local Hospitals (4)

* As a Member of the Community I would like to have a list of the local animal hospitals on the website so that I won't be asking who can spayed/neuter my animal.

### Rescues (4)

* As a Member of the Community I would like to have a list of Animal Rescues on the CPCAS website so that I can help prevent against over population in CPCAS and help animals find homes easier.

### Animal Control (4)

* As a Member of the Community I would like to have a list of the Animal Control Officers on the CPCAS website so that I won't have to call around to find the correct number or Officer on duty in my area.


## Donation

### Online Donation: PayPal (4)

* As a Member of the Community I would like to be able to donate online through CPCAS Paypal so that I won't have to come in to bring the donation and I will be able to pay with card instead of just cash or check.

### Sponsorship Opportunities (2)

* As a Member of the Community I would like to be able to view sponsorship opportunities such as adoption fees, spayed/neuter, or both so that I will know prices ahead of time and be able to donate appropriately. 

### How to Volunteer: "Donate your time!" (8)

* As a Member of the Community I would like to be able to know how to volunteer my time at CPCAS from their website, so that I won't have to call or come in to find the information/paper work.

### List of Consumables Needed (2)

* As a Member of the Community I would like to have a list of the Consumables that CPCAS uses so that I can bring in the appropriate items from my party, collection, or etc that I am holding.


# Should Have

## Lost and Found: Public

### Create Account (32)

* As the director, I would like to be able to create accounts for my employees and volunteers.

### Post Lost Animals (8)

* As a Member of the Community I would like to be able to post information/images of lost animals so that it may make it easier for the community/shelter to identify the animal.

### Post Found Animals (8)

* As a Member of the Community I would like to be able to post information/images of found animals so that it may make it easier for the community/shelter to identify the animal.

### View Current Lost Animals (8)

* As a Member of the Community I would like to be able to view information/images of lost animals so that it may make it easier for the community/shelter to identify the animal.

### View Current Found Animals (8)

* As a Member of the Community I would like to be able to use a filter to search for information/images of lost animals so that it may make it easier for the community/shelter to identify the animal.


## Lost and Found: Manager

### Manage Accounts

* As a manager at CPCAS I would like to be able to remove, edit, etc. accounts that have been made from the community.

### Manage Posts

* As a manager at CPCAS I would like to be able to remove, edit, etc. posts that have been made about lost/found animals from the community.

### View Contact Information

* As a manager of CPCAS I would like to be able to view contact information for community members who have posted lost and found animals so that we can easily reunite animals to their rightful owners.


# Could Have

## "Happy Tails"

### Share Success Stories

* As a manager of CPCAS I would like to be able to share success stories with the community about animals that have been rescued and found their forever homes so that more people may be motivated to adopt.

### Broadcast Special Events/Information - Blog

* As a manager of CPCAS I would like to be able to keep the community up-to-date with our special events and community events we attend so that more members of the community may be motivated to attend or participate.


## Adoptable Animals

### Adoption Fees

* As a Member of the Community I would like to be able to view adoption prices on CPCAS Website so that I won't have to call and ask or not be prepared when I come in to adopt.

### How to Adopt

* As a Member of the Community I would like to be able to view the adoption process on CPCAS website so that I will be more prepared for time and process of how adoption works.

### View Current available Animals/details

* As a Member of the Community I would like to be able to view the animals that are currently available for adoption as well as their description and adoption fees.


# Won't Have

## Volunteer: Public

### Clock In/Out

* As a Volunteer at CPCAS I would like to be able to Clock In/Out so that my time can more easily be tracked and more accurate.

### Schedule Yourself

* As a Volunteer at CPCAS I would like to be able to schedule myself so that the CPCAS is aware of when I will be coming in next as well as holding myself accountable for when I should be there.


## Volunteer: Manager

### View Times

* As a manager of CPCAS I would like to be able to view times that Volunteers have spent at the shelter so that I can more accurately sign off on hours of volunteer work.

### View Locations

* As a manager of CPCAS I would like to be able to view the locations that Volunteers are/where located so that if their is an incident or I need help somewhere else, I can easily direct people from their.

### View Schedules

* As a manager of CPCAS I would like to be able to view schedule of Volunteers so that I can plan days accordingly and help spread-out work loads for staff/volunteers.
